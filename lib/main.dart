import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


import 'file:///C:/Users/Ruslan/AndroidStudioProjects/Flutter/scrum_boardv2/lib/screens/main_board.dart';

void main(){
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,               // Status Bar
  ));

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
        home: MainBoard(),
    );
  }
}