import 'dart:async';
import 'dart:convert';

import 'package:scrum_boardv2/bloc/board_event.dart';
import 'package:scrum_boardv2/bloc/board_state.dart';
import 'package:scrum_boardv2/models/task.dart';
import 'package:http/http.dart' as http;

class Bloc{
  Map<int,List<Task>> allList={
    0:[],
    1:[],
    2:[],
    3:[]};

  Future<void> MoveTo({whereMove,event}) async{
    final url = 'https://scrumboard-68655.firebaseio.com/allTasks/${event.indexPage}/${event.id}.json';
    final urlTo ='https://scrumboard-68655.firebaseio.com/allTasks/$whereMove.json';
    final responseAdd = await http.post(urlTo,body: json.encode({
      'title':event.task.title,
      'description': event.task.description
    }));
    await http.delete(url);
    final extractedData = json.decode(responseAdd.body);

    allList[event.indexPage][event.indexTask].id=extractedData['name'];
    allList[whereMove].add(allList[event.indexPage][event.indexTask]);
    allList[event.indexPage].removeAt(event.indexTask);
  }

  Future<void> addTask(task) async {
    final url = 'https://scrumboard-68655.firebaseio.com/allTasks/0.json';
      final response = await http.post(url,body: json.encode({
        'title':task.title,
        'description':task.description,}));
      final extractedData = json.decode(response.body);
      allList[0].add(Task(
          title: task.title,
          description: task.description,
          id: extractedData['name']));
  }

  Future<void> getData() async{
    final url = 'https://scrumboard-68655.firebaseio.com/allTasks.json';
    final response = await http.get(url);
    final extractedData = json.decode(response.body) as List<dynamic>;
    extractMethod(extractedData, allList);
  }

  void extractMethod(extractedData,allData){
    for(int i=0;i<4;i++){
      extractedData[i].remove('const');
      extractedData[i].forEach((taskId,taskData){{
        allList[i].add(Task(
            id: taskId,
            title: taskData['title'],
            description: taskData['description']));
      }});
    }
  }

  Future<void> deleteTask(event) async{
    final url = 'https://scrumboard-68655.firebaseio.com/allTasks/${event.indexPage}/${event.id}.json';
    await http.delete(url);
    allList[event.indexPage].removeAt(event.indexTask);
  }


  Bloc() {
    _eventController.stream.listen((event) {
      _stateController.sink.addStream(mapEventToState(event));
    });
  }

  final _eventController = StreamController<BoardEvent>.broadcast();
  final _stateController = StreamController<BoardState>.broadcast();

  Stream<BoardState> get stateStream => _stateController.stream;

  Function(BoardEvent) get addEvent => _eventController.sink.add;

  Stream<BoardState> mapEventToState(BoardEvent event) async*{

    if (event is StartEvent){
      await getData();
      yield MainState(allList);
      return;
    }

    if(event is AddTaskEvent){
      await addTask(event);
      yield MainState(allList);
      return;
    }

    if(event is DeleteTask){
      await deleteTask(event);
      yield MainState(allList);
      return;
    }

    if(event is MoveTask){
      await MoveTo(whereMove: event.whereMove, event: event);
      yield MainState(allList);
      return;
    }
  }

  void dispose() {
    _eventController.close();
    _stateController.close();
  }
}

Bloc bloc = Bloc();

