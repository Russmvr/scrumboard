
import 'package:scrum_boardv2/models/task.dart';

abstract class BoardState{
  const BoardState();
}

//class StartState extends BoardState{
//  List<City> _cities;
//  StartState(this._cities);
//
//}

class MainState extends BoardState{
  Map<int,List<Task>> _allList;
  MainState(this._allList);

  List<Task> get allListBackLog{
    return [..._allList[0]];
  }

  List<Task> get allListToDo{
    return [..._allList[1]];
  }

  List<Task> get allListInProgress{
    return [..._allList[2]];
  }

  List<Task> get allListDone{
    return [..._allList[3]];
  }

}



