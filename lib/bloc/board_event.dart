import 'package:flutter/cupertino.dart';
import 'package:scrum_boardv2/models/task.dart';

abstract class BoardEvent {
  const BoardEvent();
}

class StartEvent extends BoardEvent {
  const StartEvent();
}

class AddTaskEvent extends BoardEvent {
  String title;
  String description;
  dynamic id;

  AddTaskEvent({@required this.title, this.description,this.id});
}


class MoveTask extends BoardEvent{
  int whereMove;
  int indexPage;
  int indexTask;
  dynamic id;
  Task task;

  MoveTask({
     @required this.whereMove,
     @required this.indexPage,
     @required this.indexTask,
     this.id,
     this.task});

  }

class DeleteTask extends BoardEvent {
  int indexPage;
  int indexTask;
  dynamic id;

  DeleteTask({@required this.indexPage,this.indexTask, this.id});
}
