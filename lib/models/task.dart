import 'package:flutter/material.dart';

class Task{
  dynamic id;
  final String title;     // Сама задача.
  final String description;


  Task({@required this.title, this.description, this.id});
}
