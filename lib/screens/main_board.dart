import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scrum_boardv2/bloc/board_bloc.dart';
import 'package:scrum_boardv2/bloc/board_event.dart';
import 'package:scrum_boardv2/bloc/board_state.dart';
import 'package:scrum_boardv2/models/task.dart';
import 'package:scrum_boardv2/widgets/add_task.dart';
import 'package:scrum_boardv2/widgets/gridview_tasks.dart';
import 'dart:async';

class MainBoard extends StatefulWidget {
  @override
  _MainBoardState createState() => _MainBoardState();
}

class _MainBoardState extends State<MainBoard> {
  @override
  void initState() {
//    bloc.getData().then((value) => bloc.addEvent(StartEvent()));
    bloc.addEvent(StartEvent());
    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  final List<String> _topics = ['Back Log', 'To Do', 'In Progress', 'Done'];
  PageController _pageController = PageController(initialPage: 0);
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      body: StreamBuilder<BoardState>(
          stream: bloc.stateStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              final state = snapshot.data;
              if (state is MainState) {
                Map<int, List<Task>> allListTasks = {
                  0: [...state.allListBackLog],
                  1: [...state.allListToDo],
                  2: [...state.allListInProgress],      //   --------> Я решил взять все списки задач, разбитые в четыре лиcnа(back log, to do ...).
                  3: [...state.allListDone] // Тут я как видишь достаю их со state, чтобы в дальнейшем делать логику на сравнениях indexPageView и ключами(0,1,2,3),
                            // да и вообще любых других манипуляциях
                };
                return PageView.builder(
                  controller: _pageController,
                  itemCount: 4,
                  itemBuilder: (context, indexPageView) {
                    print('СТРАНИЦА - $indexPageView');
                    return SingleChildScrollView(
                      child: Column(children: [
                        SizedBox(height: 30),
                        Row(
                          children: [
                            Container(
                                padding: EdgeInsets.only(),
                                child: IconButton(
                                  icon: Icon(Icons.add,
                                      color: Colors.grey.shade200,              //Это такая поебота. Я изначально хотел сделать Row и у него 2 child'а. Текст и кнопка
                                      size: 32),//                      Но Почему-то текст не ровно вставал в центр. Я там и Center пробовал, и Alignment, и padding и margin.
                                  onPressed: null,            //  Не помогало. Топики других колонок не стояли точно по центру, съезжали. Поэтому пошел гуглить и нашел этот лайфхак со Spacer
                                                              // Немного нехорошо, что я это не смог сделать более нормальным способом. Я уж не батя верстки. Но главное работает.
                                )),
                            Spacer(),
                            Container(
                                child: Text(
                                  _topics[indexPageView],
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontFamily: 'Raleway', fontSize: 36,),
                                )),
                            Spacer(),
                            Container(
                                padding: EdgeInsets.only(),
                                child: IconButton(
                                  icon: Icon(Icons.add,
                                      color: indexPageView == 0
                                          ? Colors.black
                                          : Colors.grey.shade200,
                                      size: indexPageView == 0 ? 32 : 32),
                                  onPressed: indexPageView == 0
                                      ? () {
                                          AddTask(context); // ---> Смотри add_task. Это добавление задачи. КНОПКА добавления задачи <---
                                        }
                                      : null,
                                )),
                          ],
                        ),
                        Divider(
                          color: Colors.black,
                          thickness: 2,
                          indent: 100,
                          endIndent: 100,
                        ),
                        Container(
                            width: width,
                            height: height-100,
                            padding: EdgeInsets.all(14),
                            child: GridViewTasks(
                                allListTasks, indexPageView, state) //  ---> Смотри gridview_tasks. Тут добавляем Gridview.builder. Т.е показываем наши задачи <---
                        ),
                      ]),
                    );
                  },
                );
              }
              return Text('asd');
            }
            if (snapshot.hasError) {
              print('ОШИБКА - ${snapshot.error}');
            }
            return Center(child: CircularProgressIndicator());
          }),
    );
  }
}
