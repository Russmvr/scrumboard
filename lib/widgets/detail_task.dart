import 'package:flutter/material.dart';
import 'package:scrum_boardv2/bloc/board_bloc.dart';
import 'package:scrum_boardv2/bloc/board_event.dart';

void detailTask(context, state,indexPageView,indexGridView,allListTasks){
  showDialog(
      context: context,
      builder: (_) => AlertDialog(
        insetPadding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(15.0))),
        content: Builder(builder: (context){
          var height = MediaQuery.of(context).size.height;
          var width = MediaQuery.of(context).size.width;
          return Container(
            height: height - 340,
            width: width,
            child: ListView(
              children: [(){
                for(var key in allListTasks.keys){
                  if(key==indexPageView){
                    return Center(child: Text(allListTasks[key][indexGridView].title,
                      style: TextStyle(fontFamily: 'Raleway', fontSize: 28, decoration: TextDecoration.underline),));
                  }
                }
              }(),
                SizedBox(height: 40,),
                    (){
                  for(var key in allListTasks.keys){
                    if(key==indexPageView){
                      return Text(allListTasks[key][indexGridView].description,
                        style: TextStyle(fontFamily: 'Raleway', fontSize: 24,),textAlign: TextAlign.center,);
                    }
                  }
                }(),
                SizedBox(height: 50,),
                Center(child: Text('Move to:',style: TextStyle(fontFamily: 'Raleway',fontSize: 20),)),
                Row(mainAxisAlignment: MainAxisAlignment.center ,children: [
                  indexPageView == 0 ? Visibility(visible: false, child: Text('gone'),):RaisedButton(child:Text('Back Log',style: TextStyle(fontSize: 17),),onPressed: (){
                    bloc.addEvent(MoveTask(
                        whereMove:0,
                        indexPage:indexPageView,
                        indexTask: indexGridView,
                        id: allListTasks[indexPageView][indexGridView].id,
                        task: allListTasks[indexPageView][indexGridView]));
                    Future.delayed(Duration(milliseconds: 900),() =>Navigator.pop(context));
                  },),
                  indexPageView == 1 ? Visibility(visible: false, child: Text('gone'),):RaisedButton(child:Text('To Do',style: TextStyle(fontSize: 17)),onPressed: (){
                    bloc.addEvent(MoveTask(
                        whereMove:1,
                        indexPage:indexPageView,
                        indexTask: indexGridView,
                        id: allListTasks[indexPageView][indexGridView].id,task:
                        allListTasks[indexPageView][indexGridView]));
                    Future.delayed(Duration(milliseconds: 900),() =>Navigator.pop(context));                                               // -----> Это всё кнопки перемещения задачи из одной колонки в другую. Также ивенты разные
                  },),
                  indexPageView == 2 ? Visibility(visible: false, child: Text('gone'),):RaisedButton(child:Text('In Progress',style: TextStyle(fontSize: 17)),onPressed: (){
                    bloc.addEvent(MoveTask(
                        whereMove:2,
                        indexPage:indexPageView,
                        indexTask: indexGridView,
                        id: allListTasks[indexPageView][indexGridView].id,
                        task: allListTasks[indexPageView][indexGridView]));
                    Future.delayed(Duration(milliseconds: 900),() =>Navigator.pop(context));
                  },),
                  indexPageView == 3 ? Visibility(visible: false, child: Text('gone'),):RaisedButton(child:Text('Done',style: TextStyle(fontSize: 17)),onPressed: (){
                    bloc.addEvent(MoveTask(
                        whereMove:3,
                        indexPage:indexPageView,
                        indexTask: indexGridView,
                        id: allListTasks[indexPageView][indexGridView].id,
                        task: allListTasks[indexPageView][indexGridView]));
                    Future.delayed(Duration(milliseconds: 900),() =>Navigator.pop(context));
                  },),
                ],),
                SizedBox(height: 20,),
                Center(child: Text('Delete this task:',style: TextStyle(fontFamily: 'Raleway',fontSize: 20),)),
                SizedBox(height: 5,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    RaisedButton(color:Colors.red,child:Text('DELETE'),onPressed: (){
                      bloc.addEvent(DeleteTask(
                          indexPage: indexPageView,
                          indexTask: indexGridView,
                          id: allListTasks[indexPageView][indexGridView].id));   // ------> Удаляем задачу. Кнопка удаления задачи
                      Future.delayed(Duration(milliseconds: 500),() =>Navigator.pop(context));
                    },),
                  ],
                ),
              ],
            ),);
        },),));
}