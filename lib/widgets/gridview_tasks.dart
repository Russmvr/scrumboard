import 'package:flutter/material.dart';
import 'package:scrum_boardv2/bloc/board_bloc.dart';
import 'package:scrum_boardv2/models/task.dart';

import 'detail_task.dart';



class GridViewTasks extends StatefulWidget {
  Map<int,List<Task>> allListTasks;
  int indexPageView;
  var state;

  GridViewTasks(this.allListTasks,this.indexPageView,this.state);

  @override
  _GridViewTasksState createState() => _GridViewTasksState();
}

class _GridViewTasksState extends State<GridViewTasks> {
  @override
  Widget build(ctx) {
    return GridView.builder(
//        physics: NeverScrollableScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          childAspectRatio: 1.05,
        ),
        itemCount: () {
          for (var key in widget.allListTasks.keys) {
            if (key == widget.indexPageView) {
              return widget.allListTasks[key].length;
            }
          }
        }(),
        itemBuilder: (context, indexGridView) {
          return Container(
            width: 30,
            height: 30,
            child: Card(
              child: InkWell(
                child: () {
                  for (var key in widget.allListTasks.keys) {
                    if (key == widget.indexPageView) {
                      return Center(
                          child: Text(widget.allListTasks[key][indexGridView].title,
                              style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                              ),
                              textAlign: TextAlign.center));
                    }
                  }
                }(),
                onTap: () {
                  detailTask(context, widget.state, widget.indexPageView,
                      indexGridView, widget.allListTasks);
                },
              ),
              elevation: 8,
              color: () {
                switch (widget.indexPageView) {
                  case 0:
                    return Color.fromARGB(255, 79, 173, 232);
                  case 1:
                    return Color.fromARGB(255, 232, 79, 79);
                  case 2:
                    return Color.fromARGB(255, 232, 148, 79);
                  case 3:
                    return Color.fromARGB(255, 79, 232, 130);
                }
              }(),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
            ),
          );
        });
  }
}
