import 'package:flutter/material.dart';
import 'package:scrum_boardv2/bloc/board_bloc.dart';
import 'package:scrum_boardv2/bloc/board_event.dart';
import 'dart:async';

Future<void> AddTask(context) async {
  String titleValue;
  String descriptionValue;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final titleFocusNode = FocusNode();

  showDialog(
      context: context,
      builder: (_) => AlertDialog(
        insetPadding: EdgeInsets.all(15),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(15.0))),
        content: Builder(
          builder: (context) {
            var height = MediaQuery.of(context).size.height;
            var width = MediaQuery.of(context).size.width;
            return Container(
              height: height - 325,
              width: width - 50,
              child: Form(
                key: _formKey,
                child: ListView(
                  children: [
                    Container(
                      alignment: Alignment.center,
                      child: Text(
                        'Add your task',
                        style: TextStyle(fontFamily: 'Raleway', fontSize: 30, fontWeight: FontWeight.w500),
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: TextFormField(
                        focusNode: titleFocusNode,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                          alignLabelWithHint: true,
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black,width: 1.1)),
                          enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black,width: 1.1)),
                          hintText:'Title',
                          hintStyle: TextStyle(
                            color: Colors.black54, fontFamily: 'Raleway', fontSize: 22,
                          ),
                        ),
                        validator: (String value){
                          if(value.isEmpty != false){
                            return "The title should not be empty";
                          }
                        },
                        onSaved: (String value){
                          titleValue = value;
                        },
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: TextFormField(
                        maxLines:5,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                          alignLabelWithHint: true,
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black,width: 1.1)),
                          enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black,width: 1.1)),
                          hintText: 'Description',
                          hintStyle: TextStyle(
                            color: Colors.black54, fontFamily: 'Raleway', fontSize: 22,
                          ),
                        ),
                        onSaved: (String value){
                          descriptionValue = value;
                        },
                      ),
                    ),
                    SizedBox(height: 30,),
                    Row(mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(width: 120,height: 40,child: RaisedButton(child:Text('Save'),onPressed: () {
                          if (!_formKey.currentState.validate()){
                            return;
                          }
                          _formKey.currentState.save();
                          bloc.addEvent(AddTaskEvent(title: titleValue, description: descriptionValue));
                          Future.delayed(Duration(milliseconds: 500),() =>Navigator.pop(context));
//                          Navigator.pop(context);
                        })),
                      ],
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ));
}